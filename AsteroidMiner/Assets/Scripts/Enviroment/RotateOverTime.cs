﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOverTime : MonoBehaviour{

    [SerializeField]
    private float _rotationSpeed = 4f;



    void FixedUpdate(){
        transform.Rotate(new Vector3(0,_rotationSpeed, 0), Space.World);
    }
}
