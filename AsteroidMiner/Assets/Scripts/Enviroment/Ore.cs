﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Health))]
public class Ore : MonoBehaviour{
    
    [SerializeField]
    private int _minContainedElements = 1;
    [SerializeField]
    private int _maxContainedElements = 10;
    [SerializeField]
    private GameObject _orePrefab;
    [SerializeField]
    private GameObject _explosionPrefab;
    [SerializeField]
    private float _radius = 1;

    Health _health;

    private void Awake(){
        _health = GetComponent<Health>();
    }

    private void Start(){
        _health.onHealthDepleted.AddListener(DestroySelf);
    }

	private void DestroySelf(){
        int spawned = Random.Range(_minContainedElements, _maxContainedElements);
        for(int i = 0; i < spawned; i++){
            Vector3 targetposition = transform.position + (Random.insideUnitSphere * _radius);
            Instantiate(_orePrefab, targetposition, Quaternion.Euler(Random.Range(0,360), 0 ,0));
        }
        Instantiate(_explosionPrefab, transform.position, Quaternion.Euler(-90, 0, 0));
        Destroy(gameObject);
	}

    private void OnDrawGizmosSelected(){
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, _radius);
    }
}
