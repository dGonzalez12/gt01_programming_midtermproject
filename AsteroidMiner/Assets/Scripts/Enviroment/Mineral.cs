﻿using UnityEngine;
using DG.Tweening;

public class Mineral : MonoBehaviour{
//There will be other minerals, they will all extend this class 
    public int value = 1;
    [SerializeField]
	private float _duration = 1;
    [SerializeField]
    Ease movementEase = Ease.OutExpo;
    [SerializeField]
    Ease scaleEase = Ease.OutSine;

	public void Collect(MineralCollector collector){
        transform.DOMove(collector.transform.position,_duration).SetEase(movementEase);
        transform.DOScale(Vector3.zero, _duration).SetEase(scaleEase).OnComplete(
            ()=> Destroy(gameObject)
        );
    }

}
