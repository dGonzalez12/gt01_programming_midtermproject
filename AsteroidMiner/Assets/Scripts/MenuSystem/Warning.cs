﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;
using DG.Tweening;

public class Warning : Submenu{

    
    [Header("Message Components")]
    [SerializeField]
    private string _message = "You've been warned! You're welcome";
    [SerializeField]
    private string _title = "Warning";

    [Header("UI components")]
    
    [SerializeField]
    private Text _titleText = null;
    [SerializeField]
    private Text _warningText = null;

    [SerializeField]
    private float _animDuration = 2f;

    [SerializeField]
    Ease easeIn = Ease.OutBack;
    [SerializeField]
    Ease easeOut = Ease.InBack;

    [SerializeField]
    private Sequence sequence;
    [SerializeField]
    private RectTransform _visualFrame;

    private Vector2 initialSizeDelta;
    [SerializeField]
    CanvasGroup canvasGroup;
    Menu menu;

    public UnityEvent onDismiss;

    private void Awake(){
        menu = FindObjectOfType<Menu>();
        initialSizeDelta = _visualFrame.sizeDelta;
    }

    private void Start(){
        _visualFrame.sizeDelta = Vector2.zero;
        // canvasGroup.interactable = false;
        canvasGroup.alpha = 0;
    }

    public void ShowWarningWrapper(string message){
        ShowWarning(message);
    }

    private void Update(){
    }

    public void ShowWarning(string message, string tilte = "Warning"){
        
        //Set values
        _titleText.text = tilte;
        _warningText.text = message;
        //Show
        menu.ShowMenu(this);
        //Animate
        sequence?.Kill();
        sequence = DOTween.Sequence();
        sequence.Append(_visualFrame.DOSizeDelta(initialSizeDelta, _animDuration * 0.75f)).SetEase(easeIn);
        sequence.Append(canvasGroup.DOFade(1,_animDuration * 0.25f));
        sequence.Play();
        sequence = null;
        canvasGroup.interactable = true;
    }

    public void DismissWarning(){
        canvasGroup.interactable = false;
        sequence?.Kill();
        sequence = DOTween.Sequence();
        sequence.Append(canvasGroup.DOFade(0,_animDuration * 0.25f));
        sequence.Append(_visualFrame.DOSizeDelta(Vector2.zero, _animDuration * 0.75f))
            .SetEase(easeOut)
            .OnComplete(
                () => menu.HideMenu(this)
            );
        sequence.Play();
        sequence = null;
        onDismiss.Invoke();
    }
}
