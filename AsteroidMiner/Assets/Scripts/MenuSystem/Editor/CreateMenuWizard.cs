﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using UnityEditor;

public class CreateMenuWizard : ScriptableWizard{
    
    public string menuName;
    public string[] submenus;
    public bool extendClass = true;

    private GameObject newMenu;
    private string componentName;
    //bool addComponent_b = false;

    private GameObject MenuPrefab;
    string menuPath = "Assets/Scripts/MenuSystem/Editor/Prefabs/Canvas.prefab";
    private GameObject SubmenuPrefab;
    string submenuPath = "Assets/Scripts/MenuSystem/Editor/Prefabs/Submenu.Prefab";
    private GameObject EventSystemPrefab;
    string eventSystemPath = "Assets/Scripts/MenuSystem/Editor/Prefabs/EventSystem.Prefab";
    private TextAsset menuClassBase;
    string classBasePath = "Assets/Scripts/MenuSystem/Editor/Prefabs/ClassBase.txt";

    [MenuItem("Menu/Create new Menu")]
    static void CreateWizard(){
    	ScriptableWizard.DisplayWizard<CreateMenuWizard>("Create New Menu", "Create");
    }

    void OnEnable(){
    	MenuPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(menuPath);
    	SubmenuPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(submenuPath);
    	EventSystemPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(eventSystemPath);
    	menuClassBase = AssetDatabase.LoadAssetAtPath<TextAsset>(classBasePath);
    }

    void OnWizardCreate(){
    	CreateNewMenu();
    }

    void CreateNewMenu(){
    	string newName = menuName.Replace(" ","");
    	newName = newName.ToLower();
    	newName = newName.Replace("menu","");
    	//Create a Menu
    	newMenu = Instantiate(MenuPrefab);
    	newMenu.transform.name = newName+"_Menu";
    	//Create submenus
    	if(submenus.Length == 0){
    		GameObject lonelySubmenu = Instantiate(SubmenuPrefab, new Vector3(0,0,0), Quaternion.identity, newMenu.transform);
    		ResetRectTransform(lonelySubmenu.GetComponent<RectTransform>() , 0);
    	}else{
    		int i = 0;
    		foreach(string submenuName in submenus){
    			GameObject newSub = Instantiate(SubmenuPrefab, new Vector3(0,0,0), Quaternion.identity, newMenu.transform);
    			newSub.transform.name = "Sub_"+submenuName;
    			ResetRectTransform(newSub.GetComponent<RectTransform>(), i);
    			i++;
    		}
    	}
    	
    	EventSystem eventTemp = FindObjectOfType<EventSystem>();
    	//Check if the event system already exists
    	if(eventTemp == null){
    		//If not create one
    		GameObject newEventSystem = Instantiate(EventSystemPrefab);
    		newEventSystem.transform.name = "EventSystem";

    	}
    	//Create a class
    	if(extendClass){
    		componentName = newName.Substring(0,1).ToUpper() + newName.Substring(1,newName.Length-1) + "Menu";
    		string newClass = menuClassBase.ToString().Replace("#ClassName#",componentName);
    		string newPath = "Assets/Scripts/"+componentName+".cs";
    		if(!File.Exists(newPath)){
    			Debug.Log("Creating custom class, Don't forget to add that to your menu!");
				using (StreamWriter outfile = new StreamWriter(newPath)){
					outfile.WriteLine(newClass);
				}
				AssetDatabase.Refresh();
				/*
    			while(!EditorApplication.isCompiling){

    			}
				AssetDatabase.Refresh();
    			//AddComponentToMenu();
    			*/
    		}else{

    			Debug.Log("Adding custom class: " + componentName);
    			//AddComponentToMenu();
    			newMenu.AddComponent(Type.GetType(componentName));
    		}
    	}else{
    		newMenu.AddComponent<Menu>();
    	}
    }

    void AddComponentToMenu(){
    	if(extendClass){
    		//Type customType = Type.GetType(componentName);
    		if(newMenu.GetComponent(Type.GetType(componentName)) == null){
    			//newMenu.AddComponent<customType>();
    			newMenu.AddComponent(Type.GetType(componentName));
    		}
    	}
    }

    void ResetRectTransform(RectTransform rectTransform , int submenuIndex){
    	Debug.Log("Width: " + Screen.width + " Height " + Screen.height);
    	rectTransform.anchoredPosition = new Vector2((Screen.width * submenuIndex) * 1.1f ,0);
    	rectTransform.sizeDelta = new Vector2(0,0);
    }
}
