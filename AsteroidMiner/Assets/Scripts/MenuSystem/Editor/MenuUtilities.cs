﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.UI;

public class MenuUtilities {

    private static GameObject SubmenuPrefab;
    static string submenuPath = "Assets/Scripts/MenuSystem/Editor/Prefabs/Submenu.Prefab";

    [MenuItem("Menu/Add Submenu")]

    static void CreateNewSubmenu(){
        SubmenuPrefab = AssetDatabase.LoadAssetAtPath<GameObject>(submenuPath);
        if(Selection.activeTransform != null){
            Menu menu = Selection.activeGameObject.GetComponent<Menu>();
            if(menu != null){
                GameObject newSub = MonoBehaviour.Instantiate(SubmenuPrefab, new Vector3(0, 0, 0), Quaternion.identity, menu.transform);
                newSub.GetComponent<RectTransform>().anchoredPosition = new Vector2((Screen.width * menu.transform.childCount) * 1.1f, 0);
                newSub.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 0);
                newSub.transform.name = "Sub_New";
            }else{
                Debug.Log("Please select a Menu object");
            }
        }else{
            Debug.Log("Please select an object");
        }
    }
}
