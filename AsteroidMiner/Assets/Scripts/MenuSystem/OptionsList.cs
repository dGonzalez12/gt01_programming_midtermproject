﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class OptionsList : MonoBehaviour{
    RectTransform content;

    int numberOfElements = 0;
    [SerializeField] float elementSize = 100;

    void Awake(){
        content = GetComponent<RectTransform>();
    }

    public void ClearList(){
        if(numberOfElements == 0){
            return;
        }
        foreach (Transform child in transform){
            Debug.Log(child.name);
            Destroy(child);
        }
        numberOfElements = 0;
    }

    public void ResizeList(){
        Vector2 size = content.sizeDelta;
        size.y = numberOfElements * elementSize;
        content.sizeDelta = size;
    }

    public void AddElement(RectTransform newElement){
        newElement.SetParent(content);
        numberOfElements ++;
    }

    public void AddToListener(UnityAction action, int index){
        content.GetChild(index).GetComponent<Button>().onClick.AddListener(action);
    }
}
