﻿using UnityEngine;
//using UnityEngine.UI;
using UnityEngine.Events;

[RequireComponent(typeof(CanvasGroup))]
public class Submenu : MonoBehaviour{
    public UnityEvent OnShow;
    public UnityEvent OnHide;

}
