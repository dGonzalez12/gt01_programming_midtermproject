﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum LoadSubmenusOption{hierarchy,findType,manual};

public class Menu : MonoBehaviour {

	[SerializeField]
	protected List<Submenu> _containedMenus;
	[SerializeField] LoadSubmenusOption _loadOption = LoadSubmenusOption.hierarchy;

	[Header("Sound Control")]
	public bool hasSound;
	public Sprite soundOn, soundOff;
	public Sprite musicOn, musicOff;

	[SerializeField] Image imageButton = null;

	public Button defaultButton;

	[Header("Scene Loading")]
	[SerializeField] Image loadBar = null;

	[Header("Screen Options")]
	[SerializeField] bool overrideScreenOrientation = false;
	[SerializeField] ScreenOrientation onStartOrientation = ScreenOrientation.Landscape;


	public void HideMenu(Submenu submenu){
		CanvasGroup canvasGroup = submenu.GetComponent<CanvasGroup> ();
		canvasGroup.alpha = 0;
		canvasGroup.blocksRaycasts = false;
		canvasGroup.interactable = false;
		submenu.gameObject.SetActive(false);
	}

	public void ShowMenu(Submenu sumbenu){
		CanvasGroup canvasGroup = sumbenu.GetComponent<CanvasGroup> ();
		canvasGroup.alpha = 1;
		canvasGroup.blocksRaycasts = true;
		canvasGroup.interactable = true;
		sumbenu.gameObject.SetActive(true);
	}

	public void ShowMenuWithAction(Submenu submenu){
		ShowMenu(submenu);
		submenu.OnShow.Invoke();
	}

	public void HideMenuWithAction(Submenu submenu){
		submenu.OnHide.Invoke();
		HideMenu(submenu);
	}

	public void InitializeMenu(Submenu menu){
		menu.GetComponent<RectTransform> ().anchoredPosition = new Vector2(0,0);
		HideMenu (menu);
	}

	public void GoToScene(string levelName){
		//normalize time scale
		Time.timeScale = 1.0f;
		DisableInteraction();
		SceneManager.LoadScene (levelName);
	}

	public void RestartThisLevel(){
		GoToScene(SceneManager.GetActiveScene().name);
	}

	public void LoadScenePretty(string sceneName){
		DisableInteraction();
		StartCoroutine(LoadSceneWithScreen(sceneName));
	}

	IEnumerator LoadSceneWithScreen(string levelName){
		
		Time.timeScale = 1.0f;
		AsyncOperation async = SceneManager.LoadSceneAsync(levelName);
		while(!async.isDone){
			if(loadBar){
				loadBar.fillAmount = Mathf.Clamp01(async.progress / 0.9f);
			}
			yield return null;
		}
	}

	public virtual void Start () {
		if(_loadOption == LoadSubmenusOption.hierarchy){
			_containedMenus = new List<Submenu>();
			for(int i = 0; i < transform.childCount; i++){
				Submenu newSubmenu = transform.GetChild(i).GetComponent<Submenu>(); 
				if(newSubmenu != null){
					_containedMenus.Add(newSubmenu);
				}
			}
		}else if(_loadOption == LoadSubmenusOption.findType){
			Submenu[] tempArraySubmenu = GetComponentsInChildren<Submenu>();
			_containedMenus = new List<Submenu>();
			for(int i = 0; i < tempArraySubmenu.Length; i++){
				_containedMenus.Add(tempArraySubmenu[i]);
			}
		}
		
		OrganizeMenus ();

		if (!PlayerPrefs.HasKey ("HasSound")) {
			PlayerPrefs.SetInt ("HasSound", 1);
			Debug.Log("Sound Key created for the first time");
			PlayerPrefs.Save ();
		}

		if(defaultButton != null){
			defaultButton.Select();
		}
		SoundCheck();

		if(overrideScreenOrientation){
			Screen.orientation = onStartOrientation;
		}
	}

	public void SoundCheck () {
		if (PlayerPrefs.GetInt ("HasSound") == 1) {
			hasSound = true;
			ActivateSound ();
		} else {
			hasSound = false;
			DeactivateSound ();
		}
	}

	public bool HasSound(){
		if (PlayerPrefs.GetInt ("HasSound") == 1) {
			hasSound = true;
		} else {
			hasSound = false;
		}
		return hasSound;
	}

	public void AlternateSound(){
		hasSound = !hasSound;
		if (hasSound) {
			ActivateSound ();
		} else {
			DeactivateSound ();
		}
	}

	public void ActivateSound(){
		AudioListener.volume = 1;
		if (imageButton != null) {
			imageButton.GetComponent<Image> ().sprite = soundOff;
		}
		PlayerPrefs.SetInt ("HasSound", 1);
		PlayerPrefs.Save ();
	}

	public void DeactivateSound(){
		AudioListener.volume = 0;
		if (imageButton != null) {
			imageButton.GetComponent<Image> ().sprite = soundOn;
		}
		PlayerPrefs.SetInt ("HasSound", 0);
		PlayerPrefs.Save ();
	}

	public void OrganizeMenus (){
		for (int i = 0; i < _containedMenus.Count; i++) {
			InitializeMenu (_containedMenus [i]);
		}
		ShowMenu (_containedMenus [0]);
	}

	public void NavigateTo(string url){
		Application.OpenURL(url);
	}

	public void SelectButton(Button selectableButton){
		if(selectableButton.interactable){
			selectableButton.Select();
		}
	}

	public void QuitGame () {
		Application.Quit();
	}

	public void DebugEvent(string eventName){
		Debug.Log("Event " + eventName + " Called at " + Time.time);
	}

	public void DisableInteraction(){
		foreach (Submenu item in _containedMenus){
			item.GetComponent<CanvasGroup>().interactable = false;
		}
	}

	public void SetScreenOrientation(ScreenOrientation newOrientation){
		Screen.orientation = newOrientation;
	}
}
