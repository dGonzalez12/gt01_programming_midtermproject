using UnityEngine;

public class LifeSpan: MonoBehaviour{

	[SerializeField]
	private float _lifeSpan = 10f;
    
	private void OnEnable(){
		Destroy(gameObject, _lifeSpan);
	}
}
