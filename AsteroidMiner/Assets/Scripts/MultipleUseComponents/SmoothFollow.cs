﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollow : MonoBehaviour{

    [SerializeField]
    private Transform _transformToFollow = null;

    [Header("Easing options")]
	[SerializeField]
    private float threshold = 0.01f;
	[SerializeField]
    private float easingSpeed = 10f;

    float distance;
    Vector3 easeVector;

    void LateUpdate(){
        if(_transformToFollow){
			if(transform.position != _transformToFollow.transform.position) {

				distance = Vector3.Distance(_transformToFollow.transform.position, transform.position);
				easeVector = new Vector3(
					GetEaseValue(transform.position.x, _transformToFollow.transform.position.x, easingSpeed * Time.deltaTime),
					GetEaseValue(transform.position.y, _transformToFollow.transform.position.y, easingSpeed * Time.deltaTime),
					GetEaseValue(transform.position.z, _transformToFollow.transform.position.z, easingSpeed * Time.deltaTime)
                );
				transform.position += easeVector;

				//Finish the easing so it is not active forever
				if (distance < threshold) {
					transform.position = _transformToFollow.transform.position;
				}
			}

        }
    }

	float GetEaseValue(float localValue, float targetValue, float easeSpeed) {
		return (targetValue - localValue) * easeSpeed;
	}
}
