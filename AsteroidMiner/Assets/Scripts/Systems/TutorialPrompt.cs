﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TutorialPrompt : MonoBehaviour{

    Warning warning;
    PlayerDetector detector;

    [SerializeField]
    private string _title;
    [SerializeField]
    private string _prompt;

    public UnityEvent onPromptDismissed;

    private void Awake(){
        warning = FindObjectOfType<Warning>();
        detector = GetComponent<PlayerDetector>();
    }

    void Start(){
        detector.onPlayerDetected.AddListener(ShowTutorialPrompt);
    }

    public void ShowTutorialPrompt(){
        string richText = _prompt.Replace("<br>", "\n");
        warning.ShowWarning(richText,_title);
        FindObjectOfType<Player>().StopMovement();
        warning.onDismiss.AddListener(FindObjectOfType<Player>().ContinueMovement);
        warning.onDismiss.AddListener(CompletePrompt);
    }

	private void CompletePrompt(){
        warning.onDismiss.RemoveAllListeners();
        onPromptDismissed.Invoke();
        Destroy(gameObject);
	}
}
