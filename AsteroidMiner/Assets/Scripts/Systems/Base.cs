﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;

public class Base : MonoBehaviour{

    public UnityEvent OnPlayerDetected;
    private Player playerInBase;
    [SerializeField]
    private Transform _stasisPoint;
    Store _store;

    private void Awake(){
        _store = GetComponent<Store>();
    }

    private void OnTriggerEnter2D(Collider2D other){
        Player player = other.GetComponent<Player>();
        if(player){
            playerInBase = player;
            playerInBase.Dock(_stasisPoint);
            _store.SetPlayer(playerInBase);
            OnPlayerDetected.Invoke();

        }
    }
    //To do: set player in store to null when launched
    
}
