﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour{
    [SerializeField] 
    private Image bar;

    Health health;

    private void Awake(){
        health = GetComponentInParent<Health>();
    }

    private void Update()
    {
        bar.fillAmount = health._health/health._maxHealth;
    }
}
