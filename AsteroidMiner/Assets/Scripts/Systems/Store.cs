using System;
using UnityEngine;
using UnityEngine.Events;

public class Store: MonoBehaviour{
	
	Player _currentPlayer;
	[SerializeField]
	private int _mineralValue = 100;

	[SerializeField]
	private int _oxygenPrice = 500;

	[SerializeField]
	[Tooltip("Price of a full tank")]
	private int _fuelPrice = 200;

	[SerializeField]
	[Tooltip("Price to repair 100% of the ship")]
	private int _repairPrice = 1000;

	//Used to update the store display
	public UnityEvent storeTransaction;
	public UnityEvent insuficientFunds;
	public UnityEvent materialsUpdated;

	public void SetPlayer(Player value){
		_currentPlayer = value;
	}

	public void SellMaterials(){
		int unitsToSell = _currentPlayer.GetComponent<Storage>()._unitsStored;
		Currency.EarnCredits(unitsToSell * _mineralValue);
		_currentPlayer.GetComponent<Storage>().ExpelUnits(unitsToSell);
		storeTransaction.Invoke();
		materialsUpdated.Invoke();
	}

	public void BuyOxygen(){
		if(!_currentPlayer){
			return;
		}

		if(Currency.CanSpend(_oxygenPrice)){
			_currentPlayer.GetComponent<OxygenSupply>().Refill();
			Currency.SpendCredits(_oxygenPrice);
			storeTransaction.Invoke();
		}else{
			insuficientFunds.Invoke();
		}
	}

	public void BuyFuel(){
		if(!_currentPlayer){
			return;
		}
		//Determine how much fuel is needed

		int fuelPrice = GetFuelPrice();

		if(Currency.CanSpend(fuelPrice)){
			Currency.SpendCredits(fuelPrice);
			_currentPlayer.GetComponent<FuelHolder>().FillFully();
			storeTransaction.Invoke();
		}else{
			insuficientFunds.Invoke();
		}
	}

	public void RepairShip(){
		if(!_currentPlayer){
			return;
		}
		//Determine how much fuel is needed
		Health hp = _currentPlayer.GetComponent<Health>();
		float deltaH = ((hp._maxHealth - hp._health)/hp._maxHealth) * 100;
		//A number from 0 to 100 that represents the percentace that needs to be fixed
		int floor = Mathf.FloorToInt(deltaH);
		if(Currency.CanSpend(Mathf.FloorToInt(_repairPrice * (deltaH/100f)))){
			_currentPlayer.GetComponent<Health>().ResetHealth();
			Currency.SpendCredits(Mathf.FloorToInt(_repairPrice * (deltaH/100f)));
			storeTransaction.Invoke();

		}else{
			insuficientFunds.Invoke();
		}
	}

	public int GetFuelPrice(){
		if(!_currentPlayer){
			return _fuelPrice;
		}else{
			FuelHolder fuel = _currentPlayer.GetComponent<FuelHolder>();
			float deltaFuel = ((fuel._tankCapacity - fuel._currentFuel)/fuel._tankCapacity);
			return Mathf.FloorToInt(deltaFuel * _fuelPrice);
		}
	}

	public int GetOxygenPrice(){
		return _oxygenPrice;
	}

	public int GetFixPrice(){
		if(!_currentPlayer){
			return _repairPrice;
		}else{
			Health hp = _currentPlayer.GetComponent<Health>();
			float deltaH = ((hp._maxHealth - hp._health)/hp._maxHealth);
			return Mathf.FloorToInt(deltaH * _repairPrice);
		}

	}
}

