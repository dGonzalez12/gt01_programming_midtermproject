using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BlackOutScreen: Submenu{

	private Image image;
	[SerializeField]
	private float _fadeTime = 2f;
	Color mainColor;
	Color alphaColor;
	[SerializeField]
	private Ease _fadeEase = Ease.OutSine;

	private void Awake(){
		image = GetComponentInChildren<Image>();
	}

	private void Start()
	{
		mainColor = image.color;
		alphaColor = mainColor;
		alphaColor.a = 0;
	}

	public void FadeOut(Action onFadeComplete){
		image.color = mainColor;
		image.DOColor(alphaColor,_fadeTime).SetEase(_fadeEase).OnComplete(
			() => FadeComplete(onFadeComplete)
		);
	}

	private void FadeComplete(Action onFadeComplete){
		onFadeComplete();
		FindObjectOfType<Menu>().HideMenu(this);
	}

	public void FadeIn(Action onFadeComplete){
		image.color = alphaColor;
		FindObjectOfType<Menu>().ShowMenu(this);
		image.DOColor(mainColor,_fadeTime).SetEase(_fadeEase).OnComplete(
			() => FadeComplete(onFadeComplete)
		);
	}

}

