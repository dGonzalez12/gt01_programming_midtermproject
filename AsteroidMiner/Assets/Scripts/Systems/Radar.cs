﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Radar : MonoBehaviour{

    [SerializeField]
    private Transform _target;
    private SpriteRenderer sprite;

    Color white = Color.white;
    Color transparent = new Color(1,1,1,0);

    [SerializeField]
    private TMP_Text _dist = null;

    [SerializeField]
    private float _minDistance;
    [SerializeField]
    private float _maxDistance;

    [SerializeField]
    private Transform _pivot;
    private Vector3 projectedPosition;

    private void Awake()
    {
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    private void OnDisable()
    {
        sprite.enabled = false;
    }

    private void OnEnable()
    {
        sprite.enabled = true;

    }

    void Update()
	{
		ApplyRotation();
		ApplyColor();
	}

	private void ApplyRotation()
	{
		projectedPosition = _target.transform.position;
		projectedPosition.z = _pivot.transform.position.z;
		_pivot.transform.LookAt(projectedPosition);
	}

	private void ApplyColor()
	{
		float distance = Vector3.Distance(_pivot.transform.position, _target.transform.position);
		sprite.color = Color.Lerp(white, transparent, Mathf.InverseLerp(_minDistance, _maxDistance, distance));
	}
}
