﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Shake : MonoBehaviour{

    public void Shake2D(float magnitude = 0.1f, float duration = 0.5f, Ease ease = Ease.OutBounce){
        Sequence sequence = DOTween.Sequence();
        
        sequence.Append(
            transform.DOLocalMove( 
                new Vector3(
                    Random.Range(-magnitude, magnitude),
                    Random.Range(-magnitude, magnitude),
                    0
                ),
                duration/10
            )
        );
        sequence.Append(
            transform.DOLocalMove(
                Vector3.zero,
                duration - duration/10
                )
        ).SetEase(ease);
        sequence.Play();
    }
}
