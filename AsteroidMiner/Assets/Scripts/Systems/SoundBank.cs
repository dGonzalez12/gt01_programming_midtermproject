﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundBank : MonoBehaviour{

    public AudioSource audioSource;
    public List<AudioClip> clips; 

    void Awake(){
        audioSource = GetComponent<AudioSource>();
    }

    public void PlayRandomSound(){
        if(clips.Count > 0){
            if(audioSource.isPlaying){
                audioSource.Stop();
            }
            audioSource.PlayOneShot(clips[Random.Range(0, clips.Count)]);
        }
    }

    void Reset(){
        audioSource = GetComponent<AudioSource>();
        audioSource.playOnAwake = false;
    }

}
