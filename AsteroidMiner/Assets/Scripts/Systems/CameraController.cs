using UnityEngine;

public class CameraController:MonoBehaviour{

	Shake shake;
	[SerializeField]
	private float _shakeDuration = 0.5f;
	[SerializeField]
	private float _shakeIntensity = 0.2f;

	private void Awake(){
		shake = GetComponentInChildren<Shake>();
	}

	public void PlayerDamage(){
		shake.Shake2D(_shakeIntensity, _shakeDuration);
	}

}
