﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(PlayerDetector))]
public class SafeZone : MonoBehaviour{

    PlayerDetector detector;
    Player player;

    private void Awake(){
        //Nice to have: Figure out how to pass parameters in events. 
        //That way having the appropiate reference to the player
        //Will allow me to make this multiplayer
        player = FindObjectOfType<Player>();

        detector = GetComponentInChildren<PlayerDetector>();
        detector.onPlayerDetected.AddListener(SetPlayerInSafeZone);
        detector.onPlayerLeft.AddListener(PlayerLeftSafeZone);
    }

    void SetPlayerInSafeZone(){
        player.SetSafe(true);
    }

    void PlayerLeftSafeZone(){
        player.SetSafe(false);
    }


}
