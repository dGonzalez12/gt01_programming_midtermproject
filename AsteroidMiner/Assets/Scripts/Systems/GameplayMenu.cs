using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameplayMenu : Menu {

    [Header("Gameplay menu")]
    [Header("HUD")]
    [SerializeField]
    private Image fuelImage = null;
    [SerializeField]
    private Image hpImageCurrent = null;
    [SerializeField]
    private Image hpImageDistance = null;
    [SerializeField]
    private Image oxygenBar = null;

    [SerializeField]
    private TMP_Text _OxygenText = null;
    [SerializeField]
	private TMP_Text _inventoryText = null;

    [Header("Store elements")]
    [SerializeField]
    private TMP_Text _fuelPrice = null;
    [SerializeField]
    private TMP_Text _fixPrice = null;
    [SerializeField]
    private TMP_Text _oxygenPrice = null;

    [Header("Inventory elements")]
    [SerializeField]
    private TMP_Text _InventoryTotalText = null;    
    

    [SerializeField]
    private Player _player;
    FuelHolder _holder;
    OxygenSupply _oxygenSupply;
    Health _health;
    Storage _inventory;
    Store _store;

    [SerializeField]
    private int currencyDisplayValue;
    private float moneyAnimDuration = 1f;
    [SerializeField]
    private TMP_Text _moneyDisplayText;

    [Header("End Game")]
    [SerializeField]
    private Submenu endScreen;
    [SerializeField]
    private TMP_Text _EndScreenResultText = null;
    

	private void Awake(){
        _player = FindObjectOfType<Player>();
        _holder = _player.GetComponent<FuelHolder>();
        _oxygenSupply = _player.GetComponent<OxygenSupply>();
        _health = _player.GetComponent<Health>();
        _inventory = _player.GetComponent<Storage>();
        _store = FindObjectOfType<Store>();
    }

    public override void Start(){
        base.Start();
        currencyDisplayValue = 0;
        UpdateMoneyDisplay();
    }

    public void OnFuelUpdated(){
        fuelImage.fillAmount = _holder._currentFuel/_holder._tankCapacity;
    }

    public void OnHealthUpdated(){
        hpImageCurrent.fillAmount = _health._health/_health._maxHealth;
    }

    private void Update(){
        oxygenBar.fillAmount = _oxygenSupply._currentOxygen/_oxygenSupply._tankCapacity;
        _OxygenText.text = $"{_oxygenSupply._currentOxygen.ToString("F1")}";
        _inventoryText.text = $"{_inventory._unitsStored}";
    }

    public void UpdateMoneyDisplay(){
        StartCoroutine("SpendRoutine", new CurrencyDisplayOptions(_moneyDisplayText, currencyDisplayValue, Currency._credits));
    }

    IEnumerator HealthRoutine(){
        yield return null;
    }

    IEnumerator SpendRoutine(CurrencyDisplayOptions options){
        yield return null;
        options.targetText.text = $"{options.initialValue}";
        for(float i  = 0; i <= moneyAnimDuration; i += Time.fixedDeltaTime){
            yield return new WaitForFixedUpdate();
            options.targetText.text = $"{Mathf.Lerp(options.initialValue, options.finalValue, i).ToString("F0")}";
        }
        // options.initialValue = options.finalValue;
        currencyDisplayValue = Currency._credits;
        options.targetText.text = $"{options.finalValue.ToString("F0")}";
    }

    public void UpdatePrices(){
        if(_store){
            _fuelPrice.text = $"x{_store.GetFuelPrice()}";
            _fixPrice.text = $"x{_store.GetFixPrice()}";
            _oxygenPrice.text = $"x{_store.GetOxygenPrice()}";
        }
    }

    public void UpdateInventory(){
        _InventoryTotalText.text = $"Total units \n x{_inventory._unitsStored}";
    }
    
    public void DisplayEndScreen(){
        ShowMenu(endScreen);
        StartCoroutine("SpendRoutine", new CurrencyDisplayOptions( _EndScreenResultText,0 , Currency._totalWinnings));
    }

    public void Pause(){
        Time.timeScale = 0.0f;
    }

    public void Unpause(){
        Time.timeScale = 1f;
    }

}

public class CurrencyDisplayOptions{
    public TMP_Text targetText;
    public int initialValue;
    public int finalValue;

    public CurrencyDisplayOptions(TMP_Text _targetText, int _initialValue, int _finalValue){
        targetText = _targetText;
        initialValue = _initialValue;
        finalValue = _finalValue;
    }
}

