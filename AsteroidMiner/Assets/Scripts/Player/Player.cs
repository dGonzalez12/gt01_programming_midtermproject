﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour{

    [Header("Multiplier")]
    [SerializeField] float collisionMultiplier = 1f;

    //Player components
    Health _health;
    PlayerMovement _movement;
    OxygenSupply _airSupply;

    [Header("Player events")]
    public UnityEvent onDockCompleted;


	public UnityEvent onDefeat;

    private Laser _laser;
    Radar _radar;

    [SerializeField]
    private float _disabledTime = 30f;

    [SerializeField]
    private BlackOutScreen blackOut = null;
    [Header("Explosion")]
    [SerializeField]
	private GameObject _explosionPrefab;
    [SerializeField]
	private int _explosions;
	private float _explosionPause = 0.5f;

	private void Awake(){
        _health = GetComponent<Health>();
        _movement = GetComponent<PlayerMovement>();
        _airSupply = GetComponent<OxygenSupply>();
        _laser = GetComponent<Laser>();
        _radar = GetComponentInChildren<Radar>();
        
    }

    private void Start(){
        _radar.enabled = false;
    }

	public void StopMovement(){
        _movement.SetMovement(false);
	}

    public void ContinueMovement(){
        _movement.SetMovement(true);
    }

	private void Update(){
        _laser.enabled = Input.GetButton("Laser");
    }

    private void OnCollisionEnter2D(Collision2D other){
        if(other.transform.GetComponent<BouncePad>()){
            return;
        }
        _health.DecreaseHealth(other.relativeVelocity.magnitude * collisionMultiplier);
    }

	public void SetSafe(bool value){
        _airSupply.enabled = !value;
    }


    //This will allow to have multilple bases and separate the docking procedures from it
	public void Dock(Transform targetBase){
        _airSupply.enabled = false;
		_movement.DockProcedure(targetBase);
	}

    public void CompleteDockProcedure(){
        onDockCompleted.Invoke();
    }

    public void Launch(){
        //Start consuming oxygen
        _airSupply.enabled = true;
        _movement.SetMovement(true);
    }

    public void Defeat(){
        _movement.SetMovement(false);
        onDefeat.Invoke();
    }


    public void OxygenDepletedRoutine(){
        //Show black screen
        blackOut.FadeIn(Defeat);
        //Once that ends, defeat
    }

    public void HPDepletedRoutine(){
        StartCoroutine("ExplosionCoroutine");
    }

    IEnumerator ExplosionCoroutine(){
        //Just in case there's 0 explosions
        yield return null;
        //Spawn 3 explosions
        Shake shake = FindObjectOfType<Shake>();
        for(int i = 0; i < _explosions; i++){
            Instantiate(_explosionPrefab, transform.position + UnityEngine.Random.insideUnitSphere , Quaternion.identity);
            yield return new WaitForSecondsRealtime(_explosionPause);
            shake.Shake2D();
        }
        //Black screen
        blackOut.FadeIn(Defeat);

    }
}
