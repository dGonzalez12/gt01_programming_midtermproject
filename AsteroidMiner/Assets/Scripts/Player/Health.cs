﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour{

    //Thinking of merging Health, Fuel, and oxygen under an extendable class "resource" since they have similar behaviours

    [SerializeField]
    public float _maxHealth = 100f;

    public float _health{get; private set;}

    public UnityEvent onHealthDepleted;
    public UnityEvent onHealthUpdated;
    public UnityEvent onDamaged;

    bool isAlive;

    private void Start(){
        ResetHealth();
    }

	public void ResetHealth(){
        isAlive = true;
        _health = _maxHealth;
        onHealthUpdated.Invoke();
	}

    public void DecreaseHealth(float value){
        _health -= value;
        _health = Mathf.Clamp(_health,0, _maxHealth);
        if(_health <= 0 && isAlive){
            isAlive = false;
            onHealthDepleted.Invoke();
        }
        onHealthUpdated.Invoke();
        onDamaged.Invoke();
    }

    public void IncreaseHealth(float value){
        _health += value;
        _health = Mathf.Clamp(_health, 0, _maxHealth);
        onHealthUpdated.Invoke();
    }
}
