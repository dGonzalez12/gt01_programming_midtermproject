﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour{

    LineRenderer lineRenderer;

    RaycastHit2D hit;
    Ore mineralOre;
    [SerializeField]
    public float maxLenght = 1f;
    [SerializeField]
    private Transform _laserStartPosition;
    [SerializeField]
    private float _laserDamage = 10;


    private void Awake(){
        lineRenderer = GetComponentInChildren<LineRenderer>();
    }

    private void OnEnable(){
        lineRenderer.enabled = true;
    }

    private void OnDisable()
    {
        lineRenderer.enabled = false;
    }

    private void Update(){
        hit = Physics2D.Raycast(_laserStartPosition.position, (Vector2) _laserStartPosition.forward, maxLenght);
        lineRenderer.SetPosition(0,_laserStartPosition.position);
        if(hit){
            lineRenderer.SetPosition(1,hit.point);
            if(hit.transform.GetComponent<Ore>()){
                hit.transform.GetComponent<Health>().DecreaseHealth(_laserDamage * Time.deltaTime);
            }
        }else{
            lineRenderer.SetPosition(1,_laserStartPosition.position + _laserStartPosition.forward * maxLenght);

        }
        
    }

    private void OnDrawGizmos(){
        Gizmos.color = Color.red;
        Gizmos.DrawLine(
            _laserStartPosition.position,
            _laserStartPosition.position + _laserStartPosition.forward * maxLenght
        );
    }

}
