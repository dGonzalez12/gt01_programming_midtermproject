﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OxygenSupply : MonoBehaviour{

    public UnityEvent OnOxygenDepleted;
    public UnityEvent onFuelUpdated;
    [Tooltip("Oxygen caoacity in seconds. If it runs out, the player loses automatically")]
    public float _tankCapacity = 100f;

	public float _currentOxygen { get; private set; }
	private bool _consumeOxigen;


    private void Start(){
		Refill();
	}

	public void Refill(){
		_currentOxygen = _tankCapacity;
	}

	private void Update(){
        ExpendOxigen(Time.deltaTime);
    }

	public void ExpendOxigen(float fuel){
		_currentOxygen -= fuel;
		_currentOxygen = Mathf.Clamp(_currentOxygen,0, _tankCapacity);
		if(_currentOxygen <= 0){
			OnOxygenDepleted.Invoke();
			//Self disable to avoid multiple calls
			this.enabled = false;
		}
		onFuelUpdated.Invoke();
	}

	public void IncreaseOxygen(float fuel){
		_currentOxygen += fuel;
		_currentOxygen = Mathf.Clamp(_currentOxygen,0, _tankCapacity);
		onFuelUpdated.Invoke();
	}

	public void ConsumeOxygen(bool value){
		_consumeOxigen = value;
	}

}
