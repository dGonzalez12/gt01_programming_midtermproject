﻿using UnityEngine;

public class Storage : MonoBehaviour{

    public int _unitsStored { get; private set;}

    private void Start()
    {
        _unitsStored = 0;
    }

    public void StoreUnits(int value){
        _unitsStored += value;
    }

    public int ExpelUnits(int value){
        if(_unitsStored >= value){
            //Since we can't sell more units than what we currently have, get exactly how many units we can decrease
            int expelledUnits = Mathf.Min(_unitsStored,value);
            _unitsStored -= expelledUnits;
            return expelledUnits;
        }else{
            return 0;
        }
    }

}
