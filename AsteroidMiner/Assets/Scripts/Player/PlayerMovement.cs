﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerMovement : MonoBehaviour{
    
    private Rigidbody2D _rigidBody;
    [SerializeField] float _speed = 0.1f;

	float _verticalInput;
	float _horizontalInput;

    [SerializeField]
	private float _damperSpeed = 0.9f;
    [SerializeField]
	private float _turnSpeed = 2f;
	private bool _brake;

	Health _health;

	FuelHolder _fuel;
	[SerializeField]
	private float _fuelExpendRate = 0.1f;

	[SerializeField]
	private bool _canControl;
	private float _dockDuration = 2f;
	[SerializeField]
	private float _fuelBrakeRate = 2f;

	[SerializeField]
	private float _fuelExpendTrheshold = 0.1f;

	private void Awake(){
        _rigidBody = GetComponent<Rigidbody2D>();
		_fuel = GetComponent<FuelHolder>();
    }

    private void Update(){
		Move();
	}

    private void FixedUpdate(){
		ApplyBrake();
	}

	private void ApplyBrake(){
		//Fixed Update setup to have frame independent braking
		if (_brake && _fuel._currentFuel >= 0){
			//Consume the input
			_brake = false;
			//ONLY expend fuel if the spasehip is fast enough
			if(_rigidBody.velocity.magnitude > _fuelExpendTrheshold){
				_fuel.ExpendFuel( _fuelBrakeRate);
			}
			_rigidBody.velocity = _rigidBody.velocity * _damperSpeed;
			_rigidBody.angularVelocity = _rigidBody.angularVelocity * _damperSpeed;
		}
	}

	private void Move(){
		if(!_canControl){
			return;
		}

        //Accelerate
		if (Input.GetKey(KeyCode.W)){
			_verticalInput = 1f;
			_fuel.ExpendFuel(_verticalInput * _fuelExpendRate * Time.deltaTime);
		}else{
			_verticalInput = 0;
		}
        //Rotate
        _horizontalInput = Input.GetAxis("Horizontal");

		_rigidBody.angularVelocity = _horizontalInput == 0 ? _rigidBody.angularVelocity : -_horizontalInput * _turnSpeed;
		if(_fuel._currentFuel > 0){
			_rigidBody.AddForce(_verticalInput * _speed * transform.up * Time.deltaTime);
		}

        //Brakes

        _brake = Input.GetKey(KeyCode.S);
	}

	public void DockProcedure(Transform targetTransform)
	{
		SetMovement(false);

		transform.DOMove(targetTransform.position, _dockDuration).OnComplete(
			OnDockComplete
		);
		transform.DOLocalRotate(targetTransform.rotation.eulerAngles, _dockDuration);
	}

	private void InstantBrake()
	{
		_rigidBody.velocity = Vector2.zero;
		_rigidBody.angularVelocity = 0f;
	}

	public void OnDockComplete(){
		GetComponent<Player>().CompleteDockProcedure();
	}

	public void SetMovement(bool value){
		_canControl = value;
		if(!_canControl){
			InstantBrake();
		}
	}
}
