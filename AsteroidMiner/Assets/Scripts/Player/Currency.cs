﻿using UnityEngine;

public class Currency : MonoBehaviour{

    //Coop style. currency will be shared between all players
    public static int _credits {get; private set;}
    public static int _totalWinnings {get; private set;}

    private void Start(){
        _credits = 0;
        _totalWinnings = 0;
    }

    public static void EarnCredits(int value){
        _credits += value;
        _totalWinnings += value;
    }

    public static bool CanSpend(int ammount){
        return ammount <= _credits;
    }

    public static void SpendCredits(int value){
        // Debug.Log($"Spent {value}");
        _credits -= value;
        _credits = Mathf.Clamp(_credits, 0, 999999);
    }

    
}
