﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour{

    [SerializeField]
    private float _minDistance = 2f;
    [SerializeField]
    private float _maxDistance = 8f;
    [SerializeField]
    private Transform _camAnchor;

    private Vector3 currentPosition;

    private void Update(){
        currentPosition = _camAnchor.localPosition;
        currentPosition.z += Input.mouseScrollDelta.y;
        currentPosition.z = Mathf.Clamp(currentPosition.z ,-_maxDistance, -_minDistance);
        _camAnchor.localPosition = currentPosition;
    }


}
