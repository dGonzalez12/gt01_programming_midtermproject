using UnityEngine;
using UnityEngine.Events;

public class CollisionDetection:MonoBehaviour{
	
	public UnityEvent collisionEntered;

	private void OnCollisionEnter2D(Collision2D other){
		
		if(other.transform.GetComponent<BouncePad>()){
			return;
		}

		collisionEntered.Invoke();
	}

}