using UnityEngine;
using UnityEngine.Events;

public class FuelHolder: MonoBehaviour{

	[SerializeField]
	public float _tankCapacity = 100f;

	public float _currentFuel { get; private set; }

	public UnityEvent onFuelDepleted;
	public UnityEvent onFuelUpdated;

	private void Start(){
		FillFully();
	}

	public void FillFully(){
		_currentFuel = _tankCapacity;
		onFuelUpdated.Invoke();
	}

	public void ExpendFuel(float fuel){
		_currentFuel -= fuel;
		_currentFuel = Mathf.Clamp(_currentFuel,0, _tankCapacity);
		if(_currentFuel <= 0){
			onFuelDepleted.Invoke();
		}
		onFuelUpdated.Invoke();
	}

	public void EarnFuel(float fuel){
		_currentFuel += fuel;
		_currentFuel = Mathf.Clamp(_currentFuel,0, _tankCapacity);
		onFuelUpdated.Invoke();
	}

}
