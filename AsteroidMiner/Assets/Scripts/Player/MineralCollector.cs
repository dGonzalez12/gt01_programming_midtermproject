﻿using UnityEngine;


public class MineralCollector : MonoBehaviour{

    Storage storage;

    private void Awake()
    {
        storage = GetComponentInParent<Storage>();
    }

    private void OnTriggerEnter2D(Collider2D other){
        Mineral mineral = other.GetComponent<Mineral>();
        if(mineral){
            storage.StoreUnits(mineral.value);
            mineral.Collect(this);
        }
    }

}
