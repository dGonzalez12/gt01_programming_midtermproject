﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerDetector : MonoBehaviour{

    public UnityEvent onPlayerDetected;
    public UnityEvent onPlayerLeft;

    public Player currentPlayer;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.GetComponent<Player>()){
            currentPlayer = other.GetComponent<Player>();
            onPlayerDetected.Invoke();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if(other.GetComponent<Player>()){
            onPlayerLeft.Invoke();
            currentPlayer = null;
        }
    }

}
