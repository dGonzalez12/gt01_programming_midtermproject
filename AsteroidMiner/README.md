ASTEROID MINER
================

## The Game Concept ##

This game was concieved as a 3 week project for Programming class in term 1 of the Advanced Game Technologies program ath the Center for entartainment arts.

The initial idea was to have a physics based space game where the user would only be able to accelerate and rotate. Movement would be limited by a fuel reserve, to make levels more challenging. However the question, "What if the player had to go back as well?". This created the idea of the player picking something up and go back.

This turned the game into a resource management game. Asteroids are scattered all around and the player can destroy them to earn resources however decisions need to be made. Will you go too far and lose sight of yout base? Will you speed up to conserve oxygen? Will you brake to avoid that collision?

### Features ###

#### Movement ####
Physics based, was part of the challenge of the game, the player has only access to 3 features. Accelerate, rotate and brake. Accelerate and brake are directly dependent on the fuel reserves. Since this is a space game, there's no air resistance and therefore, the player will keep accelerating indefenitively. Braking is almost instantaneous and it works in every direction, but this is both a gift and a curse, since braking is significantly more costly compared to accelerating.

#### HP ####
Colliding with everything (Except funny looking pink space bubblegum) causes damage to the player. This damage is proportional to the overall speed of the impact so be careful!

### Store ###
With the winnings earned from mining, the player can:
- Get a new oxygen tank
- Repair the spaceship
- Refuel the spaceship

Costs for the last two are always proportional to the damage or emptyness of the fuel tank but an oxygen tank will always cost the same.

### Radar ###
This radar will point to the base of operations. But the farthest the player is, the fainter it will look. 

### About the development ###

This game grew overall from it's initiall scope. As some of my key objectives were the better use of programming practices. Some of the highlights of the development were:
- No manager classes were used. 
- Development focused on components and reusable, multiple use classes
- A compelling game design exersize

There's some improvements and future features I'd like to add in a future version.
- More mining elements. The store/Inventory was prototyped with multiple elements, but just one was implemented
- Local multiplayer. Most of the classes avoided a class wide instance to allow for individual players to have separate stats
- An upgrade system. Larger Oxygen tanks. More reach for the laser, Less damage from collisions. Etc.
- Reworking Health, Oxygen and Fuel into a "Resource" class, since they all share similar behaviours. 
- Keep improving on software architecture, tech art, and game design overall
- An options menu
- Accesibility options (no hold buttons, remapable keys)
- Controller support


## Assets used ##
I used some of the knowledge I had in the art areas to create textures and smaller elements, as well as some previously sourced assets. Otherwise they were all free. No money was directly invested in the creation of this game.

- **[Space Depot UI](https://assetstore.unity.com/packages/2d/gui/icons/space-depot-ui-97906)**
- **[Space Skies Free](https://assetstore.unity.com/packages/2d/textures-materials/sky/spaceskies-free-80503)**
- **[Hi-Rez Spaceship Creator Free](https://assetstore.unity.com/packages/3d/vehicles/space/hi-rez-spaceships-creator-free-sample-153363)**
- **[Free Sci-Fi Textures](https://assetstore.unity.com/packages/2d/textures-materials/free-sci-fi-textures-9933)**
- **[DOTween](https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676)**
- **[Asteroids Low Poly Pack](https://assetstore.unity.com/packages/3d/environments/sci-fi/asteroids-low-poly-pack-142164)**
- **[Asteroids Pack](https://assetstore.unity.com/packages/3d/environments/asteroids-pack-84988)**
- **[Engineering Craft icons](https://assetstore.unity.com/packages/2d/gui/icons/engineering-craft-icons-71838)**
- **[Sci-Fi Sounds](https://www.gamedevmarket.net/asset/scifi-sounds-and-scifi-weapons-9632/)**